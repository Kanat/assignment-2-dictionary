NASM = nasm
ARGS = -felf -0

lib.o: lib.NASM
	$(NASM) $(ARGS) $@ $<

dict.o: dict.asm lib.o
	$(NASM) $(ARGS) $@ $<
main.o: main.asm lib.o dict.o colon.inc words.inc
	$(NASM) $(ARGS) $@ $<
prog: main.o dict.o lib.o
	ld -o $@ $<
.PHONY clean
clean:
	rm *.o