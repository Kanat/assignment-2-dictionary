%include "lib.asm"
%include "words.inc"

extern find_word
section .rodata
	overflow: db "input exceeded max length", 0
	key_not_found: db "this key isn't included in dictionary", 0
	found: db "value: ", 0

section .bss
	buf: resb 256

section .text
global _start

_start:
	.first_check:
		mov rdi, buf
		mov rsi, 256 ; buffer size
		call read_word

		cmp rax, 0
		jne .second_check
		mov rdi, overflow
		jmp .error
	.second_check:
		mov rdi, rax,
		mov rsi, first_word
		push rdx ; saving value rdx
		call find_word
		cmo rax, 0
		jne .print_value
		mov rdi, key_not_found
	.print_value:
		push rax
		mov rdi, found
		call print_string
		pop rax 	; popping value back 
		add rax, rdx
		inc rax
		mov rdi, rax
		call print_string
		mov rdi, '.'
		call print_char
		call print_newline
		xor rdi, rdi
		jmp exit

	.error:
		call print_error
		call print_newline
		mov rdi, 1
		jmp exit
