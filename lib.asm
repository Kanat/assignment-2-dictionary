section .text

global exit
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
	cmp byte[rax + rdi], 0
	je .end
	inc rax
	jmp .loop
    .end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_error:
	push rdi
	call string_length
	pop rdi
	mov rsi, rdi
	mov rdx, rax
	mov rdi, 2
	mov rax, 1
	syscall
	ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    pop rdi
    mov rdx, 1
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, 10
    mov rax, rdi
    mov rdi, rsp
    dec rdi
    push 0
    sub rsp, 20
    .loop:
	xor rdx, rdx
	div r8
	add rdx, '0'
	dec rdi
	mov [rdi], dl
	cmp rax, 0
	jnz .loop
    call print_string
    add rsp, 28
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jns .positive
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .positive:
	jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    .loop:
	mov dl, [rsi + rcx]
	cmp dl, [rdi + rcx]
	jne .false
	inc rcx
	cmp dl, 0
	jne .loop
    mov rax, 1
    jmp .return
    .false:
	xor rax, rax
    .return:
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rdx
    mov rdx, 1
    xor rdi, rdi
    xor rax, rax
    push 0
    mov rsi, rsp
    syscall
    pop rax
    pop rdx
    ret

; Если символ не является пробельным, то возвращает код символа, в противном случае -1
is_whitespace:
    cmp rax, 0x20
    je .false
    cmp rax, 0x9
    je .false
    cmp rax, 0xA
    je .false
    ret
    .false:
        mov rax, -1
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rdx, rdx
    .loop:
        mov r8, rdx
        mov r9, rdi
        mov r10, rsi
        call read_char
        call is_whitespace
        mov rsi, r10
        mov rdi, r9
        mov rdx, r8
        cmp rax, 0
        je .success
        js .whitespace
        mov [rdi + rdx], rax
        inc rdx
        cmp rdx, rsi
        jge .error
        jmp .loop
    .whitespace:
        cmp rdx, 0
        je .loop
        jmp .success
    .error:
        xor rax, rax
        ret
    .success:
        xor rax, rax
        mov [rdi + rdx], rax
        mov rax, rdi
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r8, r8
    .loop:
	mov r8b, [rdi + rdx]
	cmp r8b, '0'
	jb .return
	cmp r8b, '9'
	ja .return
	inc rdx
	sub r8b, '0'
	imul rax, 10
	add rax, r8
	jmp .loop
    .return:
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    push rcx
    call string_length
    pop rcx
    pop rdx
    pop rsi
    pop rdi
    cmp rdx, rax
    js .error
    xor rdx, rdx
    .loop:
        mov rcx, [rdi + rdx]
        mov [rsi + rdx], rcx
        inc rdx
        cmp byte[rdi + rdx], 0
        jne .loop
    xor rcx, rcx
    mov [rsi + rdx], rcx
    ret
    .error:
        xor rax, rax
        ret