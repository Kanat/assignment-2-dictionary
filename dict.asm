%include "lib.inc"

section .text
global find_word

find_word:
	xor rax, rax
	mov r8, rsi
	.loop:
		push rdi
		push rsi
		add r8, 8
		mov rsi, r8
		call string_equals
		pop rsi
		pop rdi
		cmp rax, 1
		je .success
		mov r8, [r8 - 8]
		cmo r8, 0
		jne .loop
		ret
	.success:
	mov rax, r8
	ret
